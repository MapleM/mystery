#include <iostream>
#include <vector>

using namespace std;

void print(auto A)
{
   for (auto a : A) 
        cout <<a<<" ";

   cout<<endl;
}

void mystery1(auto& Data) //insertion sort
{
  cout<<endl<<"Mystery 1"<<endl<<"---------------------"<<endl;

  for ( int i = 0 ; i < Data.size( ) ; i++)
  {
    for ( int j = 0 ; j < i ; j++)
    {
		if ( Data[ i ] < Data[ j ] )
		{
			swap( Data[ i ] , Data[ j ] );
		}
	}
    print(Data);
  }//end outer for (this brace is needed to include the print statement)

}

void mystery2(auto& Data) //bubblesort
{
  cout<<endl<<"Mystery 2"<<endl<<"---------------------"<<endl;

  for (int i = 0; i < Data.size( )-1; i++)   
  {   
    for (int j = 0; j < Data.size( )-i-1; j++)  
    {
        if (Data[j] > Data[j+1])  
        {
			swap(Data[j], Data[j+1]); 
		} 
    }

    print(Data);
  }//end outer for (this brace is needed to include the print statement)

}

void mystery3(auto& Data) //selection sort
{
  cout<<endl<<"Mystery 3"<<endl<<"---------------------"<<endl;

	int min_idx = 0;
	
	for (int i = 0; i < Data.size( )-1; i++)  
    {  
        // Find the minimum element in unsorted array  
        min_idx = i;  
        for (int j = i+1; j < Data.size( ); j++)  
        {
        if (Data[j] < Data[min_idx])  
        {
            min_idx = j;  
		}
		
        // Swap the found minimum element with the first element  
        swap(Data[min_idx], Data[i]);  
		}

    print(Data);
	}//end outer for (this brace is needed to include the print statement)

}

//... Other mysteries...

int main()
{
    
  vector<int> Data = {36, 18, 22, 30, 29, 25, 12};

  vector<int> D1 = Data;
  vector<int> D2 = Data;
  vector<int> D3 = Data;

  mystery1(D1);
  mystery2(D2);
  mystery3(D3);

}
